var gulp = require('gulp');
var prettify = require('gulp-jsbeautifier');

gulp.task('prettify', function() {
  gulp.src(['./build/**/*.css', './build/**/*.html', './build/**/*.js'])
    .pipe(prettify())
    .pipe(gulp.dest('./build'));
});
