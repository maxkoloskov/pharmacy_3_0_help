# Кардиометр

![](../pictures/settings/usi_tab.png "Вкладка «Кардиометр»")

Вкладка «Кардиометр» содержит следующие настройки:

## Выбор кардиометра

![](../pictures/settings/usi_choose_device.png)

Для установки связи с Кардиометром установите в USB-порт компьютера внешний модуль Bluetooth, включите Кардиометр и нажмите в программе кнопку обновления списка Кардиометров *![](../pictures/settings/usi_refresh.png)*.

Поиск Кардиометра может занять одну-две минуты, дождитесь его окончания. Если список Кардиометров пустой, проверьте, что Кардиометр включен, и повторите поиск.

Кардиометры, которые были выбраны ранее, отмечены галочкой. Новые Кардиометры отмечены точкой: 

![](../pictures/settings/usi_device_list.png)

Выберите Кардиометр из списка. Нажмите кнопку “Применить”. В Windows XP первое соединение с Кардиометром может занять несколько минут.

<!-- ## Сервисы

![](../pictures/Кардиометр/Сервисы.JPG)

«Сервисы» – разрешает несколько задач:
«Смена тарифа» – отправка запроса к серверу на смену тарифного плана. Кардиосервер со-общит Вам о том, что установлен новый тарифный план. Тарифные планы и запросы для их выбора приведены в публичной Оферте, размещенной на сайте micard.ru.
«Баланс Кардиометра» – запрашивает с сервера баланс счета и информацию о текущем та-рифном плане.

## Пополнить счет

![](../pictures/Кардиометр/Пополнить счет.JPG)

При нажатии на кнопку "Пополнить счет" появится форма выбора способа оплаты с 3 вариантами:
* Через платежную систему Qiwi.
* Через оператора мобильной связи Мегафон.
* Через оператора мобильной связи МТС.

![](../pictures/Кардиометр/Оплата услуг кардиосервера.JPG)

## Проверить обновления прошивки

![](../pictures/Кардиометр/Проверить обнавления прошивки.JPG)

Запрашивает с сервера версию доступной для обновления Кардиометра прошивки. После нажатия данной кнопки выводится таблица с номером подключенного Кардиометра, версией установленной на Кардиометре прошивки, а так же доступной версией для обновления. В правой части таблицы расположена кнопка «Обновить», нажатие которой запускает процесс обновления.

![](../pictures/Кардиометр/Обновление прошивки.JPG)

Во время обновления питание Кардиометра должно быть включено, а компьютер подключен к сети 220 В через бесперебойный блок питания, обеспечивающий автономную работу компьютера не менее 5 минут. Обновлять прошивку лучше с ноутбука. После окончания обновления программа проинформирует об успешном завершении операции, а Кардиометр выключится.

![](../pictures/Кардиометр/Процесс обновления прошивки.JPG)

## Востановление прошивки

![](../pictures/Кардиометр/Востановление прошивки.JPG)

Форма для восстановления прошивки Кардиометра, в случаях её повреждения.

Для запуска процесса восстановления прошивки необходимо:
1.	Перевести Кардиометр в режим загрузчика. Для этого необходимо выключить прибор – погаснут светодиоды на лицевой панели. Удерживая кнопку «Выкл», нажать кнопку «Вкл». Удерживать обе кнопки 3 секунды, светодиоды начнут быстро мигать (индикация запуска загрузчика). Отпустить обе кнопки – светодиоды продолжат мигать.
2.	Нажать кнопку «Восстановление прошивки», появится форма, отображающая процесс восстановления прошивки. После окончания восстановления программа проинформирует об успешном завершении операции. 
3.	Для выхода из режима загрузчика нажать кнопку «Выкл». Не пытайтесь выключить Кар-диометр в процессе обновления/восстановления прошивки – это может привести прибор к выходу из строя! -->