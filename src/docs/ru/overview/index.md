# Общие сведения о программе
Программа «Рабочее место врача» входит в состав комплекса «Кардиометр-МТ. КФС-01.001». Программа является основным инструментом врача для регистрации ЭКГ, ведения электронного архива ЭКГ, телеметрического контроля ЭКГ и дистанционных консультаций.

![](../pictures/overview.png "Главное окно программы")

Программа обеспечивает:
* регистрацию врачей и пациентов;
* синхронный съем ЭКГ в 12 общепринятых отведениях;
* выбор длительности регистрации ЭКГ;
* визуализацию ЭКГ в 12 общепринятых отведениях;
* передачу ЭКГ на Кардиосервер;
* просмотр архивов пациентов и их обследований;
* визуализацию типичных кардиоциклов, отведения ритма и измеренных врачебных признаков ЭКГ;
* просмотр автоматического синдромального заключения и заключения в терминах «норма - отклонение от нормы - патология» (светофор);
* анализ динамики ЭКГ;
* анализ вариабельности сердечного ритма;
* сохранение в архиве врачебного заключения;
* печать графиков ЭКГ в 12 отведениях, типичных кардиоциклов, отведения ритма, значений параметров ЭКГ и заключения врача;
* контроль работоспособности аппаратуры и качества наложения электродов.

Программа использует облачные технологии, т.е. все данные, включая записи ЭКГ, результаты автоматического анализа ЭКГ, результаты врачебного анализа ЭКГ, результаты анализа ВСР и др. по сети интернет передаются на сервер, где они хранятся сколь угодно долго. Все данные  с сервера доступны в любом месте на любом ПК, имеющим связь с интернетом.
